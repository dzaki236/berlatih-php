<?php
function tukar_besar_kecil($string){
//kode di sini
$output = '';
for($i=0; $i < strlen($string);$i++){
    if($string[$i] == strtoupper($string[$i])){
        $output .= strtolower($string[$i]);
    }else if($string[$i] == strtolower($string[$i])){
        $output .= strtoupper($string[$i]);
    }else{
        $output .= $string[$i];
    }
}
return $output;
}

// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"

//logika()
//cek $i == upper($i) output upper($i)
//cek $i == lower($i) output upper($i)
